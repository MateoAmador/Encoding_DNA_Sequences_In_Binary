# The Encoding of Nucleotides Into Bits and Back

Mateo Amador's project for the 2022 internship at the Institute for Computing in Research. Licensed under the GNU General Public License 3.0.

## Abstract

Storing data is a very important part of our modern world, so it would only make sense to store much more in a smaller space. Currently, most data if not all is stored in data centers, or warehouses filled with servers that have a processor, storage space, and memory. This method will become obsolete in the near future, being replaced by the much larger storage capabilities of DNA. For example, the DNA inside a single cell in your body can stretch up to six feet long! Also, about 5 milliliters of DNA can contain up to 1.8 zettabytes of storage. In 2021, roughly 1,300 exabytes of data was stored in data centers. If all of this data was stored with DNA, it would take a little less than a teaspoon for it to be stored.

## Background

Currently data is stored as 1's and 0's, but that could change in the near future. As I mentioned in my abstract, much of our data is stored in data centers, which take up a lot of space. Current research is being done on the storage of data in DNA and the computational powers of it. The theoretical limit of data storage in 1 gram of DNA is 455 exabytes! Because of DNA's massive storage potential, I feel that it is an important field of research that I wanted to explore, even if it was just a little bit. Since I was and am not able to synthesize DNA, I dedided that I could make a translator between nucleotide sequences and binary so a computer could interact with artificial strands of DNA. This is a very important aspect of DNA data storage because it allows any stored information in DNA to be read and used. If we develop DNA based computers, data storage would no longer be an issue, and hopefully we can develop devices that compute with DNA instead of silicon wafers.

## Application

This program acts as a translator between computers and DNA data storage devices. It translates a nucleotide sequence into a binary sequence and vice versa, so it is useful for accessing any information that is stored in an artificial DNA strand. It can also be used for the creation of a nucleotide sequence capable of storing any desired information. The only missing step is the synthesis of DNA, which I am unable to accomplish.

## Installation And Use
To install use:
```bash
git clone git@codeberg.org:MateoAmador/Project.git
```
Before compiling and executing, remember to change your directory to Encoding_DNA_Sequences_In_Binary/ and to edit the path file at the top of the function get_nucs to your correct path. Also, to run this program you need a .fasta file. A sample file is included and is named "sequence.fasta". A website that I would recommend using to get .fasta files is:
```bash
https://www.ncbi.nlm.nih.gov/
```
A tutorial to find the fasta format of what you want is here:
```bash
https://youtu.be/2TlIjPg_tIQ
```
To Compile:
```bash
gcc Encoding_Nucs_In_Binary.c -O0 -Wall -o Encoding_Nucs_In_Binary
```
Ignore any warnings, they are not errors and do not need to be addressed.
To Execute:
```bash
./Encoding_Nucs_In_Binary
```